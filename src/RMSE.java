import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RMSE{

    private final HashMap<Integer, Map<String, List>> answer_scores;
    private final AbstractSVD svd;
    private double result_rmse;

    public RMSE(AbstractSVD svd){
        answer_scores = new HashMap<Integer, Map<String, List>>();
        this.svd = svd;
    }

    public double get_result_rmse(){
        return result_rmse;
    }

    public void load_answers(){
        MongoClient m = null;
        m = new MongoClient();
        DB db = m.getDB("rec30k");
        DBCollection user_col = db.getCollection("answer");
        DBCursor cursor = user_col.find().limit(1000);
        System.out.println("Users count: " + cursor.count());

        while(cursor.hasNext()) {
            DBObject doc = cursor.next();
            Integer user_id = (Integer) doc.get("_id");
            Map<String, List> scores = (Map) doc.get("scores");
            answer_scores.put(user_id, scores);
        }
    }

    public double get_rmse() {
        load_answers();
        Float sum = 0f;
        Integer miss_count = 0;
        Integer count = 0;
        for (Map.Entry<Integer, Map<String, List>> entry : answer_scores.entrySet()) {
            Integer user_id = entry.getKey();
            Map<String, List> answers = entry.getValue();
            for (Map.Entry<String, List> user_answers : answers.entrySet()) {
                Integer item_id = Integer.parseInt(user_answers.getKey());
                Integer answer_score = (Integer) user_answers.getValue().get(0);
                Float predict_score = svd.get_predict_score_with_replace(user_id, item_id);
                if( predict_score != null){
                    sum += (answer_score - predict_score)*(answer_score - predict_score);
                    count++;
                }else{
                    miss_count++;
                }

            }
        }
        result_rmse = Math.sqrt(sum / count);
        System.out.println("Result RMSE: " + result_rmse + " Miss count: " + miss_count);
        System.out.println("Class: " + svd.getClass().getSimpleName());
        return result_rmse;

    }
}

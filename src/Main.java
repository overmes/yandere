import org.apache.commons.cli.*;

import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) throws UnknownHostException{
        Options options = new Options();

        Option db = OptionBuilder.withArgName("db")
                                 .hasArg()
                                 .isRequired()
                                 .create("db");
        Option col = OptionBuilder.withArgName("col")
                                  .hasArg()
                                  .isRequired()
                                  .create("col");
        options.addOption(db);
        options.addOption(col);

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "counter [options]", options );
        CommandLineParser parser = new GnuParser();
        try {
            CommandLine line = parser.parse( options, args );
            countSVD(line.getOptionValue("db"), line.getOptionValue("col"));
        }
        catch( ParseException exp ) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }

    }


    public static void countSVD(String db, String col){
        Matrix matrix = Util.load_data_from_list(db, col);
        AbstractSVD svd = new SVD(matrix, 50);
        svd.count();
        svd.clear_items_without_enought_scores(200);
        matrix.save_replace();
        svd.save_data();

    }

    public static void countRMSE(){
        Matrix matrix = Util.load_data_from_list_with_string_replace();
        AbstractSVD svd = new SVD(matrix, 50);
        svd.count();
        matrix.save_replace();
        RMSE rmse = new RMSE(svd);
        rmse.get_rmse();

    }
}

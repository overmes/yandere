import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Matrix {

    private ArrayList<Integer> rows_list;
    private ArrayList<Integer> cols_list;
    private ArrayList<Integer> scores_list;
    private ArrayList<Integer> dates_list;
    private final HashMap<Integer, Integer> item_index_id;
    private final HashMap<Integer, Integer> item_id_index;
    private final HashMap<String, Integer> item_name_index;
    private final HashMap<Integer, String> item_index_name;
    private final HashMap<Integer, Integer> user_index_id;
    private final HashMap<Integer, Integer> user_id_index;
    private int[] rows;
    private int[] cols;
    private int[] scores;
    private int[] dates;
    private float mean;
    private HashMap<Integer, int[]> all_users_items;
    private HashMap<Integer, Integer> users_mean_date;

    private final HashMap<Integer, Integer> items_scores_count;

    public Matrix(){
        this.rows_list = new ArrayList<Integer>();
        this.cols_list = new ArrayList<Integer>();
        this.scores_list = new ArrayList<Integer>();
        this.dates_list = new ArrayList<Integer>();
        this.user_id_index = new HashMap<Integer, Integer>();
        this.user_index_id = new HashMap<Integer, Integer>();
        this.item_id_index = new HashMap<Integer,Integer>();
        this.item_index_id = new HashMap<Integer, Integer>();

        this.items_scores_count = new HashMap<Integer, Integer>();

        this.item_index_name = new HashMap<Integer, String>();
        this.item_name_index = new HashMap<String, Integer>();

        this.all_users_items = null;
        rows = null;
        cols = null;
        scores = null;
        mean = 0;

    }

    public void setUsersItemsMap(HashMap<Integer, List<Integer>> map) {
        all_users_items = new HashMap<Integer, int[]>();
        for(Map.Entry<Integer, List<Integer>> entry : map.entrySet()){
            Integer user_index = entry.getKey();
            List<Integer> user_items = entry.getValue();
            int[] user_items_mas = ArrayUtils.toPrimitive(user_items.toArray(new Integer[user_items.size()]));
            all_users_items.put(user_index, user_items_mas);
        }

    }

    public int[] getUserItems(Integer user_index) {
        return all_users_items.get(user_index);
    }

    public Integer getItemsScoresCount(Integer item_index){
        return items_scores_count.get(item_index);
    }

    public void add_element(Integer row, Integer col, Integer score, Integer date) {
        if( items_scores_count.containsKey(col)){
            items_scores_count.put(col, items_scores_count.get(col) + 1);
        }else{
            items_scores_count.put(col, 1);
        }
        rows_list.add(row);
        cols_list.add(col);
        scores_list.add(score);
        dates_list.add(date);
    }

    public void add_next_element(Integer row, Integer col, Integer score) {
        Integer row_index = replace(row, user_id_index, user_index_id);
        Integer col_index = replace(col, item_id_index, item_index_id);
        add_element(row_index, col_index, score, null);
    }

    public void add_next_element_with_date(Integer row, Integer col, Integer score, Integer date) {
        Integer row_index = replace(row, user_id_index, user_index_id);
        Integer col_index = replace(col, item_id_index, item_index_id);
        add_element(row_index, col_index, score, null);
    }

    private Integer replace(Integer row, HashMap<Integer, Integer> id_index_map, HashMap<Integer, Integer> index_id_map) {
        Integer index;
        if( id_index_map.containsKey(row)){
            index = id_index_map.get(row);
        }else{
            index = id_index_map.size();
            id_index_map.put(row, index);
            index_id_map.put(index, row);
        }
        return index;
    }

    public int user_count(){
        return user_id_index.size();
    }

    public int item_count(){
        return item_id_index.size();
    }

    public int get_user_index(int i) {
        if (rows == null) {
            rows = ArrayUtils.toPrimitive(rows_list.toArray(new Integer[rows_list.size()]));
            rows_list = null;
        }
        return rows[i];
    }

    public int get_item_index(int i){
        if (cols == null) {
            cols = ArrayUtils.toPrimitive(cols_list.toArray(new Integer[cols_list.size()]));
            cols_list = null;
        }
        return cols[i];
    }

    public int get_score(int i){
        if (scores == null) {
            scores = ArrayUtils.toPrimitive(scores_list.toArray(new Integer[scores_list.size()]));
            scores_list = null;
        }
        return scores[i];
    }

    public int get_date(int i){
        if (dates == null) {
            dates = ArrayUtils.toPrimitive(dates_list.toArray(new Integer[dates_list.size()]));
            dates_list = null;
        }
        return dates[i];
    }

    public int get_first_date() {
        if (dates == null) {
            dates = ArrayUtils.toPrimitive(dates_list.toArray(new Integer[dates_list.size()]));
            dates_list = null;
        }
        int min = Integer.MAX_VALUE;
        for (int date : dates) {
            if (date > 0 && date < min) {
                min = date;
            }
        }
        return min;
    }

    public int get_last_date() {
        if (dates == null) {
            dates = ArrayUtils.toPrimitive(dates_list.toArray(new Integer[dates_list.size()]));
            dates_list = null;
        }
        int max = 0;
        for (int date : dates) {
            if (date > 0 && date > max) {
                max = date;
            }
        }
        return max;
    }

    public Integer get_user_mean_date(int req_user_index) {
        if( users_mean_date == null){
            users_mean_date = new HashMap<Integer, Integer>();

            HashMap<Integer, Integer[]> user_max_min_date = new HashMap<Integer, Integer[]>();
            for (int i = 0; i < this.elements_count(); i++) {
                int user_index = this.get_user_index(i);
                int date = this.get_date(i);
                if( date > 0){
                    Integer[] current = user_max_min_date.get(user_index);

                    if( current == null){
                        Integer[] temp_min_max = new Integer[]{Integer.MAX_VALUE,Integer.MIN_VALUE};
                        user_max_min_date.put(user_index, temp_min_max);
                        current = temp_min_max;
                    }

                    if( date < current[0] ){
                        Integer[] temp_min_max = new Integer[]{date,current[1]};
                        user_max_min_date.put(user_index, temp_min_max);
                    }

                    if( date > current[1] ){
                        Integer[] temp_min_max = new Integer[]{current[0],date};
                        user_max_min_date.put(user_index, temp_min_max);
                    }
                }

            }

            for (Map.Entry<Integer, Integer[]> entry : user_max_min_date.entrySet()) {
                Integer user_id = entry.getKey();
                Integer[] min_max = entry.getValue();
                users_mean_date.put(user_id, min_max[0] + (min_max[1] - min_max[0])/2);
            }
        }

        return users_mean_date.get(req_user_index);
    }

    public int elements_count() {
        if (scores == null) {
            scores = ArrayUtils.toPrimitive(scores_list.toArray(new Integer[scores_list.size()]));
            scores_list = null;
        }
        return scores.length;
    }

    public float get_mean(){
        if( mean == 0){
            float sum = 0f;
            for(int i=0; i < elements_count(); i++){
                sum += get_score(i);
            }
            mean = sum / elements_count();
            System.out.println("Mean score: " + mean);
        }
        return mean;
    }

    public void save_date(){
        Util.saveObject("userIndexReplace.dat", user_id_index);
        Util.saveObject("indexUserReplace.dat", user_index_id);
        Util.saveObject("itemIndexReplace.dat", item_id_index);
        Util.saveObject("indexItemReplace.dat", item_index_id);
        Util.saveObject("mean.dat", get_mean());
    }

    public void save_mas_data(){
        Util.saveObject("rows.dat", rows);
        Util.saveObject("cols.dat", cols);
        Util.saveObject("scores.dat", scores);
        Util.saveObject("dates.dat", dates);
    }

    public void load_mas_data(){
        System.out.println("Warning: load data from disk");
        rows = (int[]) Util.loadObject("rows.dat");
        cols = (int[]) Util.loadObject("cols.dat");
        scores = (int[]) Util.loadObject("scores.dat");
        dates = (int[]) Util.loadObject("dates.dat");
    }

    public Integer get_user_id_index_replace(Integer id){
        if(user_id_index.containsKey(id)){
            return user_id_index.get(id);
        }
        return null;
    }

    public Integer get_item_id_index_replace(Integer id){
        if(item_id_index.containsKey(id)){
            return item_id_index.get(id);
        }
        return null;
    }

    public void add_next_element_with_date_string_replace(Integer user_id, Integer item_id, Integer sub_item_id, Integer sub_score, Integer date) {
        Integer row_index = replace(user_id, user_id_index, user_index_id);

        String name = item_id.toString() + '-' + sub_item_id.toString();

        Integer col_index = replaceString(name, item_name_index, item_index_name);
        rows_list.add(row_index);
        cols_list.add(col_index);
        scores_list.add(sub_score);
        dates_list.add(date);

    }

    private Integer replaceString(String name, HashMap<String, Integer> item_name_index, HashMap<Integer, String> item_index_name) {
        Integer index;
        if( item_name_index.containsKey(name)){
            index = item_name_index.get(name);
        }else{
            index = item_name_index.size();
            item_name_index.put(name, index);
            item_index_name.put(index, name);
        }
        return index;
    }

    public void save_replace(){
        Util.saveObject("userIndexReplace.dat", user_id_index);
        Util.saveObject("indexUserReplace.dat", user_index_id);
        Util.saveObject("itemIndexReplace.dat", item_id_index);
        Util.saveObject("indexItemReplace.dat", item_index_id);
    }
}

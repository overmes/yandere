import java.util.Arrays;

public class SVDplusplus extends AbstractSVD {
    private final Matrix matrix;
    private float[] user_basis;
    private float[] item_basis;
    private final int factors_count;
    private float[][] user_p;
    private float[][] item_q;
    private final double error_stop;
    private final float mean;
    private float[][] item_y;
    private float[] user_item_y;

    public SVDplusplus(Matrix matrix, int factors) {
        this.matrix = matrix;
        this.factors_count = factors;
        this.error_stop = 0.001;
        mean = matrix.get_mean();
    }


    public void count(){
        user_basis = Util.initArray(matrix.user_count(), 0.1f);
        item_basis = Util.initArray(matrix.item_count(), 0.1f);
        user_p = Util.init2dArray(matrix.user_count(), factors_count, 0.1f);
        item_q = Util.init2dArray(matrix.item_count(), factors_count, 0.1f);
        item_y = Util.init2dArray(matrix.item_count(), factors_count, 0);

        float current_error = 1;
        float last_error = 2;
        while( Math.abs(last_error - current_error) > error_stop){
            last_error = current_error;
            current_error = make_iteration();
            System.out.println(current_error);
        }


    }

    @Override
    public void clear_items_without_enought_scores(int count) {
    }

    private float make_iteration() {
        float error_sum = 0;
        for (int i = 0; i < matrix.elements_count(); i++) {
            int user_index = matrix.get_user_index(i);
            int item_index = matrix.get_item_index(i);
            int score = matrix.get_score(i);

            //передается в get_error через поле класса
            user_item_y = count_user_y(user_index);
            float error = get_error(user_index, item_index, score);

            user_basis[user_index] = user_basis[user_index] + 0.005f*(error - 0.1f*user_basis[user_index]);
            item_basis[item_index] = item_basis[item_index] + 0.001f*(error - 0.05f*item_basis[item_index]);

            for( int f = 0; f < factors_count; f++){
                float puf = user_p[user_index][f];
                float qif = item_q[item_index][f];
                float yif = user_item_y[f];
                user_p[user_index][f] = puf + 0.005f*(error*qif - 0.1f*puf);
                item_q[item_index][f] = qif + 0.005f*(error*(puf + yif) - 0.1f*qif);

            }

            int[] user_items = matrix.getUserItems(user_index);
            if( user_items.length > 0){
                double user_items_len_sqrt = Math.sqrt( user_items.length);
                for (int user_item : user_items) {
                    for( int f = 0; f < factors_count; f++){
                        float qif = item_q[item_index][f];
                        float yif = item_y[user_item][f];
                        item_y[user_item][f] = (float) (yif + 0.0005f*((error*qif)/user_items_len_sqrt - 0.01f*yif));
                    }
                }
            }
            error_sum += error * error;

        }
        return error_sum / matrix.elements_count();
    }

    private float[] count_user_y(int user_index){
        int[] user_items = matrix.getUserItems(user_index);
        float[] user_item_y = new float[factors_count];
        Arrays.fill(user_item_y, 0);
        if( user_items.length > 0){
            for (int user_item : user_items) {
                Util.add_mas_to_second(item_y[user_item], user_item_y);
            }
            double user_items_len_sqrt = Math.sqrt( user_items.length);

            for (int j = 0; j < factors_count; j++) {
                user_item_y[j] = (float) (user_item_y[j] / user_items_len_sqrt);
            }
        }

        return user_item_y;
    }

    private float get_error(int user_index, int item_index, int score) {
        float predict_score = get_predict_score(user_index, item_index);
        return score - predict_score;
    }

    private float get_predict_score(int user_index, int item_index) {
        return mean + item_basis[item_index] + user_basis[user_index] + Util.dot(item_q[item_index],Util.summ_mas(user_p[user_index], user_item_y));

    }

    public Float get_predict_score_with_replace(int user_id, int item_id){
        Integer user_index = matrix.get_user_id_index_replace(user_id);
        Integer item_index = matrix.get_item_id_index_replace(item_id);
        if( user_index != null && item_index != null){
            return get_predict_score(user_index, item_index);
        }

        return null;
    }

    public void save_data(){
        Util.saveObject("userBasisDone.dat", user_basis);
        Util.saveObject("itemBasisDone.dat", item_basis);
        Util.saveObject("userPDone.dat", user_p);
        Util.saveObject("itemQDone.dat", item_q);
    }


}

public class SVD  extends AbstractSVD {
    private final Matrix matrix;
    private float[] user_basis;
    private float[] item_basis;
    private final Integer factors_count;
    private float[][] user_p;
    private float[][] item_q;
    private final double error_stop;
    private final float mean;

    public SVD(Matrix matrix, Integer factors) {
        this.matrix = matrix;
        this.factors_count = factors;
        this.error_stop = 0.01;
        mean = matrix.get_mean();
    }


    public void count(){
        user_basis = Util.initArray(matrix.user_count(), 0.1f);
        item_basis = Util.initArray(matrix.item_count(), 0.1f);
        user_p = Util.init2dArray(matrix.user_count(), factors_count, 0.1f);
        item_q = Util.init2dArray(matrix.item_count(), factors_count, 0.1f);

        float current_error = 1f;
        float last_error = 2f;
        while( Math.abs(last_error - current_error) > error_stop){
            long start_time = System.currentTimeMillis();
            last_error = current_error;
            current_error = make_iteration();
            long end_time = System.currentTimeMillis();
            System.out.println(current_error);
            System.out.println((end_time - start_time)/1000.0);
        }

    }

    private float make_iteration() {
        float error_sum = 0;
        for (int i = 0; i < matrix.elements_count(); i++) {
            int user_index = matrix.get_user_index(i);
            int item_index = matrix.get_item_index(i);
            int score = matrix.get_score(i);
            float error = get_error(user_index, item_index, score);

            user_basis[user_index] = user_basis[user_index] + 0.005f*(error - 0.1f*user_basis[user_index]);
            item_basis[item_index] = item_basis[item_index] + 0.001f*(error - 0.05f*item_basis[item_index]);
            for( int f = 0; f < factors_count; f++){
                float puf = user_p[user_index][f];
                float qif = item_q[item_index][f];
                user_p[user_index][f] = puf + 0.005f*(error*qif - 0.1f*puf);
                item_q[item_index][f] = qif + 0.005f*(error*puf - 0.1f*qif);

            }
            error_sum += error * error;

        }
        return error_sum / matrix.elements_count();
    }

    public void clear_items_without_enought_scores(int count){
        int items_count = matrix.item_count();
        for( int item_index = 0; item_index < items_count; item_index++){
            if( matrix.getItemsScoresCount(item_index) < count ){
                item_basis[item_index] = 0;
                for( int f = 0; f < factors_count; f++){
                    item_q[item_index][f] = 0;

                }
            }
        }
    }

    private float get_error(int user_index, int item_index, int score) {
        float predict_score = get_predict_score(user_index, item_index);
        return score - predict_score;
    }

    private float get_predict_score(int user_index, int item_index) {
        return mean + item_basis[item_index] + user_basis[user_index] + Util.dot(item_q[item_index],user_p[user_index]);

    }

    public Float get_predict_score_with_replace(int user_id, int item_id){
        Integer user_index = matrix.get_user_id_index_replace(user_id);
        Integer item_index = matrix.get_item_id_index_replace(item_id);
        if( user_index != null && item_index != null){
            return get_predict_score(user_index, item_index);
        }

        return null;
    }

    public void save_data(){
        Util.saveObject("userBasisDone.dat", user_basis);
        Util.saveObject("itemBasisDone.dat", item_basis);
        Util.saveObject("userPDone.dat", user_p);
        Util.saveObject("itemQDone.dat", item_q);
        Util.saveObject("mean.dat", mean);
    }


}

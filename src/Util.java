import com.mongodb.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.util.*;

public class Util {
    public static float[][] init2dArray(Integer width, Integer height, float range) {
        float[][] mas = new float[width][height];
        for( int i = 0; i < width; i++){
            mas[i] = initArray(height, range);
        }
        return mas;
    }

    public static float[] initArray(Integer size, float range) {
        float mas[] = new float[size];
        for( int i = 0; i < size; i++){
            mas[i] = (float) (Math.random()*2*range - range);
        }
        return mas;
    }

    public static Matrix load_data(){
        MongoClient m = null;
        m = new MongoClient();
        DB db = m.getDB("yandere");
        DBCollection user_col = db.getCollection("users");
        DBCursor cursor = user_col.find(new BasicDBObject("len", new BasicDBObject("$gte", 20)));
        System.out.println("Users count: " + cursor.count());

        Matrix matrix = new Matrix();
        while(cursor.hasNext()) {
            DBObject doc = cursor.next();
            Integer user_id = (Integer) doc.get("_id");
            Map<String, Integer> scores = (Map) doc.get("scores");
            for (Map.Entry<String, Integer> entry : scores.entrySet()) {

                Integer item_id = Integer.parseInt(entry.getKey());
                Integer score = entry.getValue();
                matrix.add_next_element(user_id, item_id, score);

            }
        }
        return matrix;
    }

    public static Matrix load_data_from_list(String dbName, String colName){
        MongoClient m = null;
        m = new MongoClient();
        DB db = m.getDB(dbName);
        DBCollection user_col = db.getCollection(colName);
//        DB db = m.getDB("rec30k");
//        DBCollection user_col = db.getCollection("test");
        DBCursor cursor = user_col.find(new BasicDBObject("len", new BasicDBObject("$gte", 30).append("$lte", 1000)));
//        {'completed':2, 'dropped':4, 'plan to watch':6, 'watching':1, 'on-hold':3}
//        List access_types = new ArrayList<Integer>(Arrays.asList(2,6,1,3));
//        DBCursor cursor = user_col.find();
        System.out.println("Users count: " + cursor.count());
        HashMap<Integer, List<Integer>> all_users_items = new HashMap<Integer, List<Integer>>();
        Matrix matrix = new Matrix();
        while(cursor.hasNext()) {
            DBObject doc = cursor.next();
            Integer user_id = (Integer) doc.get("_id");
            List user_items = new ArrayList<Integer>();

            Map<String, List> scores = (Map) doc.get("scores");

            float sum = 0;
            float count = 0;

            for (Map.Entry<String, List> entry : scores.entrySet()) {
                List<Integer> score_list = entry.getValue();
                Integer score = score_list.get(0);
                if( score > 0){
                    sum += score;
                    count += 1;
                }

            }

            float avg = sum / count;
            if( 5 < avg && avg < 9){

                for (Map.Entry<String, List> entry : scores.entrySet()) {

                    Integer item_id = Integer.parseInt(entry.getKey());
                    List<Integer> score_list = entry.getValue();
                    Integer score = score_list.get(0);
                    Integer type = score_list.get(1);
                    Integer date = score_list.get(2);
                    if( score > 0){
                        matrix.add_next_element_with_date(user_id, item_id, score, date);
                    }

//                if( access_types.contains(type)){
//                    int item_index = matrix.get_item_id_index_replace(item_id);
//                    user_items.add(item_index);
//                }

                }
                int user_index = matrix.get_user_id_index_replace(user_id);
                all_users_items.put(user_index, user_items);

            }


        }
        matrix.setUsersItemsMap(all_users_items);
        all_users_items = null;
        System.out.println(String.format("Scores count: %s", matrix.elements_count()));
        return matrix;
    }

    public static Matrix load_data_from_list_with_string_replace(){
        MongoClient m = null;
        m = new MongoClient();
        DB db = m.getDB("rec30k");
        DBCollection user_col = db.getCollection("test");

        List access_types = new ArrayList<Integer>(Arrays.asList(2,6,1,3));
        DBCursor cursor = user_col.find();
        System.out.println("Users count: " + cursor.count());
        HashMap<Integer, List<Integer>> all_users_items = new HashMap<Integer, List<Integer>>();
        Matrix matrix = new Matrix();
        while(cursor.hasNext()) {
            DBObject doc = cursor.next();
            Integer user_id = (Integer) doc.get("_id");

            Map<String, List> scores = (Map) doc.get("scores");
            for (Map.Entry<String, List> entry : scores.entrySet()) {

                Integer item_id = Integer.parseInt(entry.getKey());
                List<Integer> score_list = entry.getValue();
                Integer score = score_list.get(0);
                Integer type = score_list.get(1);
                Integer date = score_list.get(2);


                for (Map.Entry<String, List> subEntry : scores.entrySet()) {
                    Integer sub_item_id = Integer.parseInt(subEntry.getKey());
                    List<Integer> sub_score_list = subEntry.getValue();
                    Integer sub_score = sub_score_list.get(0);
                    Integer sub_type = sub_score_list.get(1);
                    Integer sub_date = sub_score_list.get(2);

                    if( sub_score > 0 && date != 0 && sub_date != 0 && (date < sub_date) ){
                        matrix.add_next_element_with_date_string_replace(user_id, item_id, sub_item_id, sub_score, date);
                    }
                }


            }
            System.out.println("Users: " + user_id);
        }
        all_users_items = null;
        System.out.println(String.format("Scores count: %s", matrix.elements_count()));
        return matrix;
    }

    public static void saveObject(String fileName, Object list){
        try {
            FileOutputStream fout = new FileOutputStream("data/" + fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(list);
            oos.close();
        }
        catch (Exception e) { e.printStackTrace(); }
    }

    public static Object loadObject(String fileName){
        try {
            FileInputStream fin = new FileInputStream("data/" + fileName);
            ObjectInputStream ois = new ObjectInputStream(fin);
            Object data = ois.readObject();
            ois.close();
            return data;
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }


    public static float dot(float[] mas1, float[] mas2) {
        float mult = 0;
        for (int i=0; i<mas1.length;i++) {
            mult += mas1[i] * mas2[i];

        }
        return mult;
    }

    public static void add_mas_to_second(float[] mas1, float[] mas2){
        for (int i=0; i<mas1.length;i++) {
            mas2[i] = mas1[i] + mas2[i];

        }
    }

    public static float[] summ_mas(float[] mas1, float[] mas2) {
        float[] sum = new float[mas1.length];
        for (int i = 0; i < mas1.length; i++) {
            sum[i] = mas1[i] + mas2[i];

        }
        return sum;
    }

    public static float[] multimas(float[] mas, float digit){
        float[] multi = new float[mas.length];
        for (int i = 0; i < mas.length; i++) {
            multi[i] = mas[i]*digit;

        }
        return multi;
    }
}

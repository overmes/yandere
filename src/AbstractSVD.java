public abstract class AbstractSVD {

    public abstract Float get_predict_score_with_replace(int user_id, int item_id);
    public abstract void count();
    public abstract void clear_items_without_enought_scores(int count);
    public abstract void save_data();


}

public class timeSVD extends AbstractSVD {
    private final Matrix matrix;
    private float[] user_basis;
    private float[] item_basis;
    private final Integer factors_count;
    private float[][] user_p;
    private float[][] item_q;
    private final double error_stop;
    private final float mean;
    private final int time_item_beans_count;
    private final int first_date;
    private final int step;
    private final int last_date;
    private float[] alfa;
    private float dev_u_t;
    private final int time_user_beans_count;
    private final int user_step;
    private float[][] user_time_basis;
    private float[][] item_time_basis;
    private float[][] user_time_p;

    public timeSVD(Matrix matrix, Integer factors) {
        this.matrix = matrix;
        this.factors_count = factors;
        this.error_stop = 0.001;
        mean = matrix.get_mean();
        time_item_beans_count = 4;
        time_user_beans_count = 20;
        first_date = matrix.get_first_date();
        last_date = matrix.get_last_date();
        step = (last_date - first_date) / time_item_beans_count;
        user_step = (last_date - first_date) / time_user_beans_count;
    }

    private int get_time_bean(int date,int step){
        int res = 0;
        int date_plus_steps = first_date + step;
        while( res < time_item_beans_count -1 && date > date_plus_steps){
            res++;
            date_plus_steps+= step;

        }
        return res;
    }

    public void count(){
        user_basis = Util.initArray(matrix.user_count(), 0.1f);
        user_time_basis = Util.init2dArray(matrix.user_count(), time_user_beans_count, 0);
        item_basis = Util.initArray(matrix.item_count(), 0.1f);
        item_time_basis = Util.init2dArray(matrix.user_count(), time_item_beans_count, 0);
        user_p = Util.init2dArray(matrix.user_count(), factors_count, 0.1f);
        user_time_p = Util.init2dArray(matrix.user_count(), factors_count, 0);
        item_q = Util.init2dArray(matrix.item_count(), factors_count, 0.1f);
        alfa = Util.initArray(matrix.user_count(), 0);

        float current_error = 1f;
        float last_error = 2f;
        while( Math.abs(last_error - current_error) > error_stop){
            last_error = current_error;
            current_error = make_iteration();
            System.out.println(current_error);
        }


    }

    @Override
    public void clear_items_without_enought_scores(int count) {
    }

    private float make_iteration() {
        float error_sum = 0;
        for (int i = 0; i < matrix.elements_count(); i++) {
            int user_index = matrix.get_user_index(i);
            int item_index = matrix.get_item_index(i);
            int score = matrix.get_score(i);
            int date = matrix.get_date(i);
            int item_time_bean = get_time_bean(date, step);
            int user_time_bean = get_time_bean(date, user_step);

            float error = get_error(user_index, item_index, score, date);

            //def_u_t from error count
            alfa[user_index] = alfa[user_index] + 0.000005f*(error*dev_u_t - 100*alfa[user_index]);
            float ub = user_basis[user_index];
            float ub_time = user_time_basis[user_index][user_time_bean];
            user_basis[user_index] += 0.005f*(error - 0.1f*ub);
            user_time_basis[user_index][user_time_bean] += 0.0005f*(error - 10f*ub_time);

            float ib = item_basis[item_index];
            float ib_time = item_time_basis[user_index][item_time_bean];
            item_basis[item_index] += 0.001f*(error - 0.05f*ib);
            item_time_basis[user_index][item_time_bean] += 0.00001f*(error - 10f*ib_time);


            for( int f = 0; f < factors_count; f++){
                float puf = user_p[user_index][f];
                float qif = item_q[item_index][f];
                user_time_p[user_index][f] += 0.00001f*(error*(puf + dev_u_t*user_time_p[user_index][f]) - 100*user_time_p[user_index][f]);
                user_p[user_index][f] = puf + 0.005f*(error*qif - 0.1f*puf);
                item_q[item_index][f] = qif + 0.005f*(error*puf - 0.1f*qif);

            }
            error_sum += error * error;

        }
        return error_sum / matrix.elements_count();
    }

    private float get_error(int user_index, int item_index, int score, int date) {
        float predict_score = get_predict_score(user_index, item_index, date);
        return score - predict_score;
    }

    private float get_predict_score(int user_index, int item_index, int date) {
        int item_time_bean = get_time_bean(date, step);
        int user_time_bean = get_time_bean(date, user_step);
        Integer user_mean_date = matrix.get_user_mean_date(user_index);
        if( user_mean_date == null){
            user_mean_date = first_date;
        }
        dev_u_t = (float) (Math.signum(date - user_mean_date) * Math.pow(Math.abs(date - user_mean_date)/86400, 0.4));
        float ub =  user_basis[user_index] + alfa[user_index] * dev_u_t + user_time_basis[user_index][user_time_bean];
        float ib = item_basis[item_index] + item_time_basis[user_index][item_time_bean];
        return mean + ib + ub + Util.dot(item_q[item_index],Util.summ_mas(user_p[user_index], Util.multimas(user_time_p[user_index], dev_u_t)));

    }

    public Float get_predict_score_with_replace(int user_id, int item_id){
        Integer user_index = matrix.get_user_id_index_replace(user_id);
        Integer item_index = matrix.get_item_id_index_replace(item_id);
        if( user_index != null && item_index != null){
            return get_predict_score(user_index, item_index, last_date);
        }

        return null;
    }

    public void save_data(){
        Util.saveObject("userBasisDone.dat", user_basis);
        Util.saveObject("itemBasisDone.dat", item_basis);
        Util.saveObject("userPDone.dat", user_p);
        Util.saveObject("itemQDone.dat", item_q);
    }


}
